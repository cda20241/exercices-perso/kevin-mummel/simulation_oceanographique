from Fonctions import*

print("Hello world")

point_haut(table)

list_immerges = point_toujours_immerges(table)

cinq_occurences_capteur = valeurs_repetees(list_immerges, 5)
print(f"Capteur(s) toujours immergé(s) : {cinq_occurences_capteur}")

list_non_immerges = point_non_immerges(table)

cinq_occurences_capteur = valeurs_repetees(list_non_immerges, 5)
print(f"Capteur(s) jamais immergé(s) : {cinq_occurences_capteur}")

date_debut = input("Entrer une date de début: (entre 2022-01-01-02 et 2022-01-02-12):")
date_fin = input("Entrer une date de fin entre: (2022-01-01-02 et 2022-01-02-12):")

lignes_selectionnees = extraire_lignes_entre_dates(list_immerges, date_debut, date_fin)

occurrences_N_list = compter_occurrences_N(lignes_selectionnees)

pourcentages_N = {'1': 100.0, '2': 100.0, '3': 80.0, '5': 60.0, '6': 40.0, '4': 20.0, '9': 20.0}

user_input = input("Entrer un numéro de capteur (1-9) : ")

if user_input in pourcentages_N:
    sensor_number = user_input
    pourcentage = pourcentages_N[sensor_number]
    print(f"Le pourcentage de temps pendant lequel le capteur {sensor_number} est immergé est de : {pourcentage:.2f}%")
else:
    print("Capteur non trouvé dans les données ou entrée invalide.")

date_list_immerges = date_list_immerges(list_immerges)

date_coef_max = date_max_immerges(date_list_immerges)
print("le plus gros cefficient de marée est le ", date_coef_max)

date_coef_min = date_min_immerges(date_list_immerges)
print("le plus petit cefficient de marée est le ", date_coef_min)