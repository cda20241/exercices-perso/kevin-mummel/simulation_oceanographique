import csv

# Lecture du fichier CSV et création d'un dictionnaire
table = []
with open('marees_v1.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
    for row in reader:
        table.append(row)


def point_haut(table):
    """
    A partir de la liste table issue du fichier csv, on affiche seulement
    les positions des capteurs actif en y=2

    :return: print
    """
    for row in table:
        if row['Actif'] == "1" and row['X'] == "2" and row['Y'] == "0":
            return print("Marée au point le plus haut à x=0 et y=2")
        elif row['Actif'] == "1" and row['X'] == "2" and row['Y'] == "1":
            return print("Marée au point le plus haut à x=1 et y=2")
        elif row['Actif'] == "1" and row['X'] == "2" and row['Y'] == "2":
            return print("Marée au point le plus haut à x=2 et y=2")


def point_toujours_immerges(table):
    """
    A partir de la liste "table" issue du fichier "marées_V1.csv", on créer une nouvelle liste
    avec seulement les données correspondantes aux capteurs immergés
    :return: "list_immerges"
    """
    list_immerges = []
    for row in table:
        if row['Actif'] == "1":
            list_immerges.append(row)
    return list_immerges


def valeurs_repetees(list_immerges, seuil_repetition=5):
    """
    A partir de la liste "list_immerges", on compte les capteurs qui sont
    immergés 5 fois (le nombres de jours total)
    :return: "cinq_occurences_capteur"
    """
    valeurs = {}
    result = []

    # Compter le nombre de répétitions de chaque valeur de la colonne 'N'
    for row in list_immerges:
        valeur_N = row['N']
        if valeur_N in valeurs:
            valeurs[valeur_N] += 1
        else:
            valeurs[valeur_N] = 1

    # Ajouter les valeurs de répétition à la liste de résultats
    for valeur_N, repetitions in valeurs.items():
        if repetitions >= seuil_repetition:
            result.append(valeur_N)

    return result


def point_non_immerges(table):
    """
    A partir de la liste "table" issue du fichier "marées_V1.csv", on créer une nouvelle liste
    avec seulement les données correspondantes aux capteurs NON immergés
    :return: "list_non_immerges"
    """
    list_non_immerges = []
    for row in table:
        if row['Actif'] == "0":
            list_non_immerges.append(row)
    return list_non_immerges


def valeurs_repetees(list_non_immerges, seuil_repetition=5):
    """
    A partir de la liste "list_non_immerges", on compte les capteurs qui sont
    NON immergés 5 fois (le nombres de jours total)
    :return: cinq_occurences_capteur
    """
    valeurs = {}
    result = []

    # Compter le nombre de répétitions de chaque valeur de la colonne 'N'
    for row in list_non_immerges:
        valeur_N = row['N']
        if valeur_N in valeurs:
            valeurs[valeur_N] += 1
        else:
            valeurs[valeur_N] = 1

    # Ajouter les valeurs qui se répètent au moins 'seuil_repetition' fois à la liste de résultats
    for valeur_N, repetitions in valeurs.items():
        if repetitions >= seuil_repetition:
            result.append(valeur_N)

    return result


def extraire_lignes_entre_dates(list_immerges, date_debut, date_fin):
    """
    Extraire de "list_immerges" les lignes compris entre les 2 dates rentrées par l'utilisateur
    :return: lignes_selectionnees
    """
    lignes_entre_dates = []
    for row in list_immerges:
        date_row = row['Date']
        if date_debut <= date_row <= date_fin:
            lignes_entre_dates.append(row)
    return lignes_entre_dates


def compter_occurrences_N(lignes_selectionnees):
    """
    Compter l'occurence des capteurs N
    :return:occurrences_N_list
    """
    occurrences_N = {}
    for row in lignes_selectionnees:
        valeur_N = row['N']
        if valeur_N in occurrences_N:
            occurrences_N[valeur_N] += 1
        else:
            occurrences_N[valeur_N] = 1
    return occurrences_N


def pourcentage_occurrences(occurrences_N_list):
    """
    Faire le pourcentage d'occurence des capteurs selon le nombre de jours
    choisi par l'utilisateur
    :return: pourcentages_N
    """
    pourcentages = {}
    for valeur_N, occurrences in occurrences_N_list.items():
        pourcentage = (occurrences / nombre_de_dates) * 100
        pourcentages[valeur_N] = pourcentage

    return pourcentages


def date_list_immerges(list_immerges):
    """
    A partir de la list des capteurs immergés, on compte l'occurence des dates dans une liste
    :return: date_list_immerges
    """
    date_list_immerges = {}
    for row in list_immerges:
        valeur_date = row['Date']
        if valeur_date in date_list_immerges:
            date_list_immerges[valeur_date] += 1
        else:
            date_list_immerges[valeur_date] = 1
    return date_list_immerges


def date_max_immerges(date_list_immerges):
    """
    On garde la date qui a l'occurence la plus grande
    :return: date
    """
    valeur_max = max(date_list_immerges)
    return valeur_max


def date_min_immerges(date_list_immerges):
    """
    On garde la date qui a l'occurence la plus petite
    :return: date
    """
    valeur_min = min(date_list_immerges)
    return valeur_min
